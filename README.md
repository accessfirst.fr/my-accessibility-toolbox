My accessibility toolbox: Tools and techniques I use for manual accessibility testing. 

[[_TOC_]]

## List of used tools

### ANDI (Accessible Name and Description Inspector)

Installable as a Bookmarklet. 

Home page: https://www.ssa.gov/accessibility/andi/help/install.html

Possible uses for accessibility testing: 

* Control of accessibility properties
* Images alternatives
* Color contrasts
* Links
* Content structures

Select the needed module in the drop-down menu, located at the top left corner of the overlay.

*Important note*: results aren't updated automatically when the DOM changes. Requires a manual refresh (circular arrow in the top right corner).



### Developer tools - accessibility tree inspector

Embedded feature of browsers.

* In Firefox it's an additional panel that must be manually activated, at the same level as the Elements Inspector 
* In Chrome, it's a subpanel of the Elements inspector
* In Safari, it's a group of properties in the Nodes subpanel, of the Elements panel

*Recommended: Firefox*. It displays the whole accessibility tree (as opposed to the current leaf only in other browsers), provides on-the-fly guidance, and accessibility properties can be read from the context menu in the main page (provided there are accessibility properties in the current element).

Possible uses for accessibility testing: 

* Images alternatives
* Control of accessibility properties
* Control of DOM modification
* Forcing of element state (hover, active, etc.)
* DOM modification to simulate user actions / use cases

### Stylus extension

Browser extension to easily define and manage client-side CSS rules (a.k.a. user styles).

Home page: https://github.com/openstyles/stylus/wiki 

Possible uses for accessibility testing:

* Highlight elements of a given type (links, paragraphs, lists, etc.)
* Highlight elements with given properties (elements with an id or a title attribute, etc.)
* Display the values (and their variations) of non-visible attributes (like aria-expanded)

### Web developer toolbar

Free Extension for Firefox, or Chrome. Allows to manipulate the page’s code, and fetch information from it.

Home page: http://chrispederick.com/work/web-developer/ 

Possible uses for accessibility testing:

* Outline elements (headings, tables, images with various properties, etc.)
* Display pages without CSS
* Display ARIA roles
* Display title, alt attributes
* Resize window at precise dimensions

### HeadingsMap

Free Extension for Firefox. Displays the headings outline, and highlights incorrect structure (gaps, missing content).

Home page: https://addons.mozilla.org/en-US/firefox/addon/headingsmap/



### Colour Contrast Analyser

Free stand-alone app to check contrast ratio (colour picker).

Home page: http://www.paciellogroup.com/resources/contrastanalyser/ 

Possible uses for accessibility testing: pixel-by-pixel contrast checking (useful for heterogeneous backgrounds).


## How to check, by test topic

### Accessible name

To detect elements with no accessible name: use ANDI and check the list of Accessibility Alerts.

To find out the accessible name of focusable elements

* with ANDI: select the "Focusable elements" module, and hover over the element with the mouse, or navigate the list of elements (can be refined by type)
* with the Accessibility tree Inspector: check the Name property

To find out the accessible name of non-focusable elements

* with the Accessibility tree Inspector: check the Name property

### Accessible description

To find out the accessible description of focusable elements

* with ANDI: select the "Focusable elements" module, and hover over the element with the mouse, or navigate the list of elements (can be refined by type). The output text for description is in a different colour.
* with the Accessibility tree Inspector: check the Description property

To find out the accessible description of non-focusable elements

* with the Accessibility tree Inspector: check the Description property

### Presence and boundaries of elements

* with Stylus: create a user style that displays the tag name in the `::before` pseudo-element, and draws a border around the element's box.

Example: to show all occurrences of the `P` element:

```css
p {
    border: 3px rgba(255, 0, 0, 0.25) solid;
}

p::before {
    content: "<p>";
    background: rgba(255, 0, 0, 0.25);
    color: black;
    display:block;
}
```


### Value (and changes) of attributes

* with the Elements Inspector: display the Inspector windows and the browser window (where the page is displayed) next to each other, with the inspected elements in the viewport for both windows. Observe the variations of values in the DOM Inspector when events occur in the browser window
* with Stylus: create a user style that displays the value of the attribute in the `::before` pseudo-element. Example: to display the value of `aria-expanded`:

```css

[aria-expanded]::before {
    content: attr(aria-expanded);
    background:yellow;
    color: black;
    display:block;
    position:absolute;
    top:0;
    left:0;
}

[aria-expanded] {
    background:yellow;
    border: 2px yellow solid;
}

[aria-expanded=true] {
    background:green;
    border: 2px green solid;
}
```

### Headings outline

* with HeadingMaps: click the extension button and check the outline. Checks for both `<hn>` tags and tags with `role=heading`. Note: as of v. 3.8.1, it is possible to not display hidden headings: in the Settings, uncheck _Hidden headers_. In case of doubt, double-check with the DOM inspector or the Web Developer Toolbar
* with the Web Developer Toolbar: _Outline > Show Element Tag Names_, then _Outline > Outline Headings_. Caution: doesn't take element with `role=heading` into account. Double-check with HeadingMaps, or with a user style targeting those elements and displaying the value of `aria-level`

### Image alternative

To check the computed accessible name of images: use ANDI and its graphics/images module. However, ANDI simulates the behaviour of recent assistive technologies (AT). In some implementations, this may be not representative of the behaviour of older ATs. Only the `alt` attribute should be considered for those ones.

To check the value of the `alt` attribute: either use the DOM inspector, or the Web Developer Toolbar (_Images > Display alt attributes_). You may need to disable CSS first, in order to solve display issues, and more easily distinguish plain images from image-links.

### Keyboard support

Use the Tab key to navigate in a page, and test the keyboard accessibility of every focusable element. Also test backwards navigation (Shift+Tab keys). If the focus ring isn't visible enough, use an enhanced focus style (eg. with Stylus: `*:focus {outline: 3px lightgreen solid !important;}`).


Some interface elements are collections of options that are browsed with the arrow keys, not the Tab key. This includes:

* Options in a drop-down list (standard `<select>`, or custom _Listbox_ widget)
* Groups of radio buttons
* Tabs in a tab list

For these elements, the container is the keyboard-focusable element, i.e. reached with the Tab key. Pressing Tab again should result in going to the next element in tab order, not to the next available option.

Most interactive elements are activated with the Enter key. However, some elements are activated by the Space bar: radio buttons, checkboxes (standard or custom). Buttons are activated by both.

On most elements that trigger the display of an overlay (be it a menu, a tooltip, or a pop-in), the Esc key is used to dismiss the current element, without registering user actions; and the focus is returned to the element that had focus before the overlay. Example: if a search field has a drop-down list for suggested values, pressing the Esc key must close the drop-down list, leave the search field in its initial state (no selection made), and return the focus to the field.

In the case of nested elements with overlays, only the top-most one is dismissed. Example: a custom tooltip can be displayed on a button inside a pop-in. Pressing Esc when the focus is on the tooltip must close the tooltip, and return the focus to the button; and NOT close the  pop-in. On the other hand, if the focus is anywhere else in the pop-in, pressing Esc MUST close the pop-in without retaining user actions, and return the focus to the element that triggered the display of the pop-in.

If the element that has focus is removed, then the focus must be set on an element that seems logical, from a keyboard user point of view. Example: a button that removes the current sample. Pressing the button will result in removing the whole block from the DOM, including the button, which necessarily has focus. In this case, the focus must be set to either the nearest item in the list of samples; or to a text indicating that the samples list is empty (if that's the case).

### Code validity check

Assistive technologies query the accessibility tree, which is generated by the browser based on the generated HTML code. The generated source code is the result of the interpretation of the downloaded source code, modified by the CSS and the JS code linked with the HTML.

To ensure a correct generation of the accessibility tree, the generated HTML code must validate, at least for the following rules:

* the syntax is correct (writing and tag nesting rules have been respected)
* the mandatory tags (like `<title>` in `<header>`) and mandatory attributes (like `alt` for `<img/>`) are present
* existing attributes are used on the allowed tags only (for example, it is not allowed to set an `alt` attribute on other tags than `<img/>`, `<input type=image/>`, and `<area>` that are children of a `<map>`)
* every `id` in the document is valid, and unique
* when used, the ARIA attributes respect the specifications (ARIA attributes are allowed on certain elements only)

There is a tolerance for:

* attributes that don’t exist in the HTML specification (they are ignored by the screen readers)
* elements that don’t exist in the HTML specification (they are considered as neutral elements by the screen readers)
* errors related to HTML Entities in URLs

Code validity can be checked by running validators, like [Validator.nu](https://validator.nu/), proposed by the W3C. When the page can't be accessed from the outside via its URL, it is still possible to feed the validator with the HTML code of the page.

To ease the process, you can use this feature through a bookmarklet. Steps to create a bookmarklet:

1. Create a new bookmark
2. Paste the following code in the URL field of the bookmark:

```js
javascript:(function(){function c(a,b){var c=document.createElement("textarea");c.name=a;c.value=b;d.appendChild(c)}var e=function(a){for(var b="",a=a.firstChild;a;){switch(a.nodeType){case Node.ELEMENT_NODE:b+=a.outerHTML;break;case Node.TEXT_NODE:b+=a.nodeValue;break;case Node.CDATA_SECTION_NODE:b+="<![CDATA["+a.nodeValue+"]]>";break;case Node.COMMENT_NODE:b+="<!--"+a.nodeValue+"-->";break;case Node.DOCUMENT_TYPE_NODE:b+="<!DOCTYPE "+a.name+(a.publicId?' PUBLIC "'+a.publicId+'"':"")+(!a.publicId&&a.systemId? " SYSTEM":"")+(a.systemId?' "'+a.systemId+'"':"")+">\n"}a=a.nextSibling}return b}(document),d=document.createElement("form");d.method="POST";d.action="http://validator.w3.org/check";d.enctype="multipart/form-data";d.target="_blank";c("fragment",e);c("doctype","Inline");c("group","0");document.body.appendChild(d);d.submit()})(); 
``` 

3. Name the bookmark and place it in a convenient location (like the bookmarks bar of your browser).

Clicking on it will open a new tab with the result of the check for the current page. Note that the interface will vary with the doctype (HTML4 or XHTML versus HTML5).

## Step-by-step testing techniques

The techniques described here apply to Firefox. Other browsers may have different behaviours and settings.

In Firefox, the accessibility panel must be activated in the developer Tools (off by default, for performance reasons). 

Once it's done, the icon turns green. The accessibility tree is then visible and explorable. There's also an additional item in the contextual menu to directly check the accessibility properties of the right-clicked element.



### AN-01: Check the accessible name of an element with the Accessibility Inspector

1. Select the right element to inspect
    1. Place the mouse cursor over the element, and click on the right button
    1. Choose Inspect Element in the context menu
    1. Click on the targeted element in the Inspector to highlight it
   Note: these steps are needed when the targeted element has a complex content, because the browser may inspect not the element, but one of its children.
1. Display the accessibility properties view for this element
    1. From the Inspector view, right-click on the highlighted element
    1. Choose Inspect Accessibility Properties in the context menu
   Note: elements with no role and no text node will have no accessibility properties, thus the option will be greyed
1. Check the accessible name: Name property in the Properties section


### AR-01: Check the accessible role of an element with the Accessibility Inspector

1. Select the right element to inspect
    1. Place the mouse cursor over the element, and click on the right button
    1. Choose Inspect Element in the context menu
    1. Click on the targeted element in the Inspector to highlight it
   Note: these steps are needed when the targeted element has a complex content, because the browser may inspect not the element, but one of its children.
1. Display the accessibility properties view for this element
    1. From the Inspector view, right-click on the highlighted element
    1. Choose Inspect Accessibility Properties in the context menu
    Note: elements with no role and no text node will have no accessibility properties, thus the option will be greyed
1. Check the accessible role: Role property in the Properties section


### AD-01: Check the accessible description of an element with the Accessibility Inspector

1. Select the right element to inspect
    1. Place the mouse cursor over the element, and click on the right button
    1. Choose Inspect Element in the context menu
    1. Click on the targeted element in the Inspector to highlight it
    Note: these steps are needed when the targeted element has a complex content, because the browser may inspect not the element, but one of its children.
1. Display the accessibility properties view for this element
    1. From the Inspector view, right-click on the highlighted element
    1. Choose Inspect Accessibility Properties in the context menu
    Note: elements with no role and no text node will have no accessibility properties, thus the option will be greyed
1. Check the accessible description: Description property in the Properties section


### CI-xx: Check Image alternatives

The difficulty of this test is that there are several ways to implement an image, to implement its alternative, and the value of the alternative depends on the context of use. The specs, if any, should provide detailed information regarding the expected alternatives, in order to reduce uncertainty when testing.

In this document, we focus on this implementation technique, found on most websites:

* `<img>` tags, with `alt` and/or `title` attributes

Decorative images should have an empty alternative. An image is considered as decorative if it can be removed from the content without any loss of information. This includes images that contain information, that is already conveyed by other pieces of content (texts or alternatives of other images) in the same context.

### CI-01: Check Image alternatives with ANDI

1. Activate the user styles defined in the _SA-01: Show Links_, and _SB-01: Show Buttons_ techniques
1. Start ANDI
1. Select the _graphics/images_ module
1. If needed, you can activate alternatively the _hide xx inline_, _yy decorative inline_, _hide zz background_, _find ww background_ controls to ease the visual analysis, depending on what you need to check
1. Either hover the images highlighted in the page content, or use the arrows to browse the collection of images in the page content. At this stage, ignore the images that belong to links and buttons, which are highlighted after step 1 is applied
1. Check the computed alternative of each image in the section of ANDI named _ANDI output_
1. To check the images ignored at step 5, select the _links/buttons_ module, and check _ANDI output_. The tool computes the accessible name of the link or button, which must be relevant with regards of its function

*Important note*: ANDI simulates the behaviour of recent assistive technologies (AT). In some implementations, this may be not representative of the behaviour of older ATs. Only the `alt` attribute should be considered for those ones. To check the value of the `alt` attribute: apply _CI-02_ or _CI-03_.

### CI-02: Check the `alt` attribute with the DOM Inspector

1. Select the right element to inspect
    1. Place the mouse cursor over the image, and click on the right button
    1. Choose Inspect Element in the context menu
    1. Click on the targeted image (`<img>` tag) in the Inspector to highlight it
   Note: these steps are needed when the targeted image is in a complex content structure, because the browser may inspect not the image, but one of its parents or siblings.
1. Check if the image has the attribute `aria-hidden="true"` (tip: you can also check that the Inspect Accessibility Properties option is greyed in the context menu, whereas the Accessibility panel is active).
    1. If yes, the test is over, because this image will not be output to screen readers, hence the test is not applicable
    1. If no, check the `alt` attribute. It must be present, even if it has no value, or its value is the empty string 

### CI-03: Check the `alt` attribute with the Web Developer Toolbar

The following operations highlight images with and without `alt` attributes, inside or outside links or buttons, and display the values of the `alt` attributes, if any. The tester must assess if these values are appropriate alternatives for the concerned images.

1. Click the extension button to display the menus
1. Click on _CSS_, then _Disable All Styles_
1. Activate the user styles defined in the _SA-01: Show Links_, and _SB-01: Show Buttons_ techniques
1. Click on _Images_, then _Outline Images Without Alt Attributes_
1. Click on _Images_, then _Display alt attributes_

### _AA-xx_: Check values or highlight elements with Stylus

To create a custom user style with Stylus:

1. Open the extension menu
1. Click on Manage
1. Click on Write new style
1. Enter a name (tip: choose one that is easy to recognise, with a prefix that allows easy sorting (stylrd are sorted by alphabetical order; when there are lots of custom styles, it saves a lot of time)
1. Enter CSS code in the "Code 1" area
1. Save the created style

To use it: activate the style on the inspected page, and observe the changes in values, if it's the expected result.

Tip: choose colours that are very different from the tested user interface, to make sure the added content are easily detectable.

### AA-01: Check the value of `aria-expanded` with Stylus

This is useful when you need to see how this value changes on an expandable widget, like a menu, when it is used.

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
[aria-expanded]::before {
content: attr(aria-expanded);
background:yellow;
color:black;
display:block;
position:absolute;
top:0;
left:0;
}

[aria-expanded] {
background:yellow;
border: 2px yellow solid;
}

[aria-expanded=true] {
background:green;
border: 2px green solid;
}
```

### AA-02: Check the value of `aria-hidden` with Stylus

This is useful when you need to see which element will not be read by screen readers (i.e. `aria-hidden=true`; any other value is equivalent to `false`).

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
[aria-hidden] {
    border: 5px blue solid;
    background-color: lightblue;
    font-size:10px;
}

[aria-hidden]::before {
    content: attr(aria-hidden);
    border: 1px black solid;
    color: black ;
    background-color: lightyellow;
    font-size:10px;
}
```

### AA-03: Check the value of `aria-selected` with Stylus

This is useful when you need to see how this value changes on a widget with selectable elements, like tabs, when it is used.

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
[aria-selected]::before {
content: attr(aria-selected);
background:yellow;
color:black;
display:block;
position:absolute;
top:0;
left:0;
}

[aria-selected] {
background:yellow;
border: 2px yellow solid;
}

[aria-selected=true] {
background:green;
border: 2px green solid;
}
```

### AA-04: Check the value of `aria-haspopup` with Stylus

This is useful when you need to see where this attribute is declared, and its value, when it is used on widgets like menu buttons.

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
[aria-haspopup]::before {
content: attr(aria-haspopup);
background:yellow;
color:black;
display:block;
position:absolute;
top:0;
left:0;
}

[aria-haspopup] {
background:yellow;
border: 2px yellow solid;
}
```

### AA-05: Check the value of `aria-describedby` with Stylus

This is useful when you need to see where this attribute is declared, and its value, when it is used on elements like input fields or buttons.

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:


```css
[aria-describedby]::before {
content: "aria-describedby: " attr(aria-describedby);
background:rgba(0,0,255,0.1);
color:darkblue;
display:block;
position:absolute;
bottom:0;
left:0;
}

[aria-describedby] {
background:rgba(0,0,255,0.1);
border: 2px darkblue solid;
}
```

### AA-06: Check the value of `aria-labelledby` with Stylus

This is useful when you need to see where this attribute is declared, and its value, when it is used on elements like input fields or buttons.

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:


```css
[aria-labelledby]::before {
content: "aria-labelledby: " attr(aria-labelledby);
background:rgba(255,0,0,0.1);
color:darkred;
display:block;
position:absolute;
bottom:0;
left:0;
}

[aria-labelledby] {
background:rgba(255,0,0,0.1);
border: 2px darkred solid;
}
```

### AA-07: Check the value of `aria-checked` with Stylus

This is useful when you need to see how this value changes on a widget with checkable elements, like checkboxes, when it is used.

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
[aria-checked]::before {
content: attr(aria-checked);
background:yellow;
color:black;
display:block;
position:absolute;
top:0;
left:0;
}

[aria-checked] {
background:yellow;
border: 2px yellow solid;
}

[aria-checked=true] {
background:green;
border: 2px green solid;
}
```


### FF-01: Force focus on every element

This is useful when you need to control the position of the focus ring, when it is hard to detect, or is not fixed yet.

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
*:focus {
outline: 3px lightgreen solid !important;
}
```

### LF-01: Locate the keyboard focus

1. Open the Console
1. Type `document.activeElement`

### SA-01: Show links

This is useful when you need to see what elements are links, when it's not obvious. It is also useful to sort out images that are part of a link, which impacts their alternatives.

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
a, [role=link] {
border: 3px rgba(255, 0, 0, 0.25) solid;
}

a::before, [role=link]::before {
content: "link";
background: rgba(255, 0, 0, 0.25);
color: black;
display:block;
}
```

### SB-01: Show buttons

This is useful when you need to see what elements are buttons, when it's not obvious. It is also useful to sort out images that are part of a button, which impacts their alternatives.

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
button, [type=button], [type=image], [role=button] {
border: 3px rgba(0, 255, 0, 0.75) solid;
}

button::before, [type=button]::before, [type=image]::before, [role=button]::before {
content: "button";
background: rgba(0, 255, 0, 0.75);
color: black;
display:block;
}
```

### SB-02: Show `<a>` elements with `role=button`

This is useful when you need to see which anchor links (`<a>`) have been assigned a role of button.

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
a[role="button"] {
border: 3px rgba(0, 150, 55, 0.5) solid;
}

a[role="button"]::before {
content: "a[role=button]";
background: rgba(0, 150, 55, 0.5);
color: black;
display:block;
}
```

### SP-01: Show paragraphs

This is useful when you need to check that paragraphs are used (paragraphs are needed for correct structuration, whereas DIVs and SPANs may be "missed" by users of screen readers). It can also be used to check that empty paragraphs are not used to create margins or blank space. 

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
p {
border: 3px rgba(255, 0, 0, 0.25) solid;
}

p::before {
content: "<p>";
background: rgba(255, 0, 0, 0.25);
color: black;
display:block;
}
```

### SL-01: Show lists

This is useful when you need to check that lists are correctly implemented, of each different type, including those defined through the `role` attribute. 

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:


```css
ol {
border: 3px rgba(100, 100, 0, 0.5) solid;
}

ol::before {
content: "<ol>";
background: rgba(100, 100, 0, 0.5);
color: black;
display:block;
}

ul {
border: 3px rgba(100, 0, 255, 0.5) solid;
}

ul::before {
content: "<ul>";
background: rgba(100, 0, 255, 0.5);
color: black;
display:block;
}

dl {
border: 3px rgba(100, 255, 255, 1) solid;
}

dl::before {
content: "<dl>";
background: rgba(100, 255, 255, 1);
color: black;
display:block;
}

[role=list]::before {
content: "list";
border: 1px black solid;
color: black ;
background-color: lightyellow;
font-size:12px;
}
[role=listitem]::before {
content: "listitem";
border: 1px black solid;
color: black ;
background-color: lightyellow;
font-size:10px;
}


/* detect LIs that are not direct children of a UL or an OL */

* > li {
    border: 3px red solid;
}

ul > li, ol > li {
    border: none;
}

```

### SL-02: Show landmark roles

This is useful when you need to see what elements are (mandatory) landmarks defined through the `role` attribute, and what are their boundaries.

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
[role="main"] {
border: 3px rgba(255, 10, 255, 0.5) solid;
}

[role="main"]::before {
content: "role=main";
background: rgba(255, 10, 255, 0.5);
color: black;
display:block;
}

[role="navigation"] {
border: 3px rgba(0, 110, 255, 0.5) solid;
}

[role="navigation"]::before {
content: "role=navigation";
background: rgba(0, 110, 255, 0.5);
color: black;
display:block;
}

[role="contentinfo"] {
border: 3px rgba(0, 200, 255, 0.85) solid;
}

[role="contentinfo"]::before {
content: "role=contentinfo";
background: rgba(0, 200, 255, 0.85);
color: black;
display:block;
}
[role="banner"] {
border: 3px rgba(200, 200, 0, 0.85) solid;
}

[role="banner"]::before {
content: "role=banner";
background: rgba(200, 200, 0, 0.85);
color: black;
display:block;
}
[role="search"] {
border: 3px rgba(200, 0, 100, 0.85) solid;
}

[role="search"]::before {
content: "role=search";
background: rgba(200, 0, 100, 0.85);
color: black;
display:block;
}
```

### SL-03: Show landmark tags

This is useful when you need to see what elements are (mandatory) landmarks, defined by tags, and what are their boundaries.

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
footer {
border: 3px rgba(0, 0, 255, 0.25) solid;
}

footer::before {
content: "<footer>";
background: rgba(0, 0, 255, 0.25);
color: black;
display:block;
}

header {
border: 3px rgba(0, 100, 255, 0.5) solid;
}

header::before {
content: "<header>";
background: rgba(0, 100, 255, 0.5);
color: black;
display:block;
}

main {
border: 3px rgba(0, 255, 255, 0.5) solid;
}

main::before {
content: "<main>";
background: rgba(0, 255, 255, 0.5);
color: black;
display:block;
}

nav {
border: 3px rgba(0, 255, 0, 0.25) solid;
}

nav::before {
content: "<nav>";
background: rgba(0, 255, 0, 0.25);
color: black;
display:block;
}
```


### SL-04: Show `alert|status|log` roles

This is useful when you need to check which element have these roles. 

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
[role=alert]::before {
content: "alert";
border: 1px black solid;
color: black ;
background-color: lightyellow;
font-size:10px;
}
[role=status]::before {
content: "status";
border: 1px black solid;
color: black ;
background-color: lightyellow;
font-size:10px;
}
[role=log]::before {
content: "log";
border: 1px black solid;
color: black ;
background-color: lightyellow;
font-size:10px;
}
```

### SL-05: Show article roles & tags

This is useful when you need to check which element have the role of an article. 

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
article, *[role=article] {
    border: 3px rgba(255, 0, 0, 0.25) solid;
}

article::before, *[role=article]::before {
    content: "<article>";
    background: rgba(255, 0, 0, 0.25);
    color: black;
    display:block;
}

*[role=article]::before {
    content: "role=article";
}
```

### SQ-01: Show quotes

This is useful when you need to check that quotes are correctly implemented, of each different type. 

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
q {
border: 3px rgba(100, 100, 0, 0.5) solid;
}

q::before {
content: "<q>";
background: rgba(100, 100, 0, 0.5);
color: black;
display:block;
}

blockquote {
border: 3px rgba(100, 0, 255, 0.5) solid;
}

blockquote::before {
content: "<blockquote>";
background: rgba(100, 0, 255, 0.5);
color: black;
display:block;
}
```

### SF-01: Show fieldset and legend tags

This is useful when you need to check that `<fieldset>` and `<legend>` tags are correctly used, and what are their boundaries.

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
fieldset {
border: 3px rgba(100, 100, 0, 0.5) solid;
}

fieldset::before {
content: "<fieldset>";
background: rgba(100, 100, 0, 0.5);
color: black;
display:block;
top:0;

}
legend {
border: 3px rgba(0, 100, 0, 0.5) solid;
}

legend::after {
content: "<legend>";
background: rgba(0, 100, 0, 0.5);
color: black;
display:block;
bottom:0;
}
```

### ST-01: Show `title` attributes for links

This is useful when you want to check the values of `title` attributes on links. It also highlights links with an empty `title` attribute (which is prohibited for accessibility). 

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
a[title]::before {
content: attr(title);
border: 1px black solid;
color: black ;
background-color: lightyellow;
font-size:10px;
}
a[title=""]::before {
content: "empty";
border: 1px red solid;
color: white ;
background-color: red;
font-size:10px;
}
```

### SA-02: Show `id` attributes

This is useful when you want to check the values of `id` attributes on any element. It can be easily adapted to restrict it to certain elements only. 

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
[id]::before {
content: attr(id);
border: 1px black solid;
color: black ;
background-color: lightyellow;
font-size:10px;
}
```

### SA-03: Highlight elements with lang attributes with a given value

This is useful when you want to check if elements have a `lang` attribute of a given value. The example below is for the values starting with "`en`", to take into account declarations of regional suffixes, like "`en-US`". 

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
[lang*="en"] {
outline: 3px rgba(0, 255, 255, 1) solid;
}
```

### SA-04: Highlight links with `target` attributes with the value `\_blank`

This is useful when you want to check if links open a new window.

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
[target="_blank"] {
border: 3px rgba(255, 0, 255, 0.5) solid;
}

[target="_blank"]::before {
content: "target=_blank";
background: rgba(255, 0, 255, 0.5);
color: black;
display:block;
}
```

### SA-05: Highlight links to downloadable office files

This is useful when you want to check if some links download an office document, whose accessibility shall be controlled.

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
[href*=".pdf"], [href*=".doc"], [href*=".xls"], [href*=".ppt"] {
border: 3px rgba(255, 0, 255, 0.5) solid;
}

[href*=".pdf"]::before, [href*=".doc"]::before, [href*=".xls"]::before, [href*=".ppt"]::before {
content: "downloadable";
background: rgba(255, 0, 255, 0.5);
color: black;
display:block;
}
```
### SA-06: Display `lang` attributes

This is useful when you want to check the value of `lang` attributes. For each element that has a declared `lang` attribute, its value will appear in a caption tied to it. Note that since this attribute is inherited, all descendant elements bear the same characteristic, unless  their own `lang` attribute has a different value. 

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
[lang] {
    outline: 3px rgba(255, 0, 255, 1) solid;
}
[lang]:before {
    content: attr(lang);
    background:lightyellow;
    color: black;
    border: 1px black solid;
    font-style:italic;
    padding: 0 0.5em;
}
```

### SH-01: Check the heading outline with HeadingMaps

Before using the extension for testing, ensure that it is configured to ignore hidden headings: in Settings, uncheck _Hidden headers_.

1. Click the extension button to visualise the outline
1. Check for missing headings
    1. skipped levels
    1. texts that have the appearance of headings but don't appear in the outline
    1. headings that would be needed to have a coherent structure
1. Check for excessive headings (texts marked up as headings, whereas they shouldn't be)

If you see headings in the outline that do not appear in the page content, they are either visually hidden, or removed from the flow (check them with the Dev Tools, or _SH-02_). In the latter case, ignore them to assess the relevance of the outline.


### SH-02: Check the heading outline with the Web Developer Toolbar

This technique lacks an important feature, since it doesn't take into account the elements with `role=heading`. It is kept here for reference only.

1. Click the extension button to display the menus
1. Click on _Outline_, then _Show Element Tag Names_ (if it's not already checked)
1. Click on _Outline_, then _Outline Headings_
1. Activate _SH-03_ as a complement, for elements with `role=heading`

### SH-03: Display the role and level of elements with `role=heading` 

This technique is useful to visualize headings in their context. However, it doesn't provide a clear view of the actual headings tree, since visual aspect may differ from source order and elements relationships.


Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
[role=heading]::before {
    content: "heading" attr(aria-level);
    border: 1px black solid;
    color: black ;
    background-color: lightyellow;
    font-size:10px;
}

[role=heading],
h1, h2, h3, h4, h5, h6
{
    border: 1px black solid;
    color: darkred ;
    background-color: lightyellow;
}

h1::before, h2::before, h3::before, h4::before, h5::before, h6::before {
    border: 1px black solid;
    color: black ;
    background-color: lightyellow;
    font-size:10px;
}


h1::before {
    content: "H1";
}

h2::before {
    content: "H2";
}

h3::before {
    content: "H3";
}

h4::before {
    content: "H4";
}

h5::before {
    content: "H5";
}

h6::before {
    content: "H6";
}

[role=heading] {
    border: 1px black solid;

    background-color: lightyellow;
}

```



### PE-01: Highlight prohibited elements and attributes

This technique is useful to check if elements or attributes that generate an accessibility violation are present in the page. Elements are outlined in red, attributes in pink.

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
basefont, blink, center, font, marquee, s, strike, tt, big {
border: 3px rgba(255, 0, 0, 1) solid;
}

[align], [alink], [background], [bgcolor], [border], [cellpading], [cellspacing], [char], [charoff], [clear], [compact], [color], [frameborder],[hspace], [link], [marginheight], [marginwidth], [text], [valign], [vlink], [vspace], [size] {
border: 3px pink solid;
}

input[size], img[width], img[height], svg[width], svg[height], canvas[width], canvas[height], embed[width], embed[height], object[width], object[height] {
border:none;
}
```


### DT-01: Display table elements and properties

This technique is useful to detect and visualize table elements, their boundaries, and some properties.

Apply steps in _AA-xx_, and paste this code in the "Code 1" area:

```css
table {
    border: 3px rgba(0,255, 0, 0.25) solid;
}

table::before {
    content: "<table>";
    background: rgba(0,255, 0, 0.25);
    color: black;
    display:block;
}

table[role=presentation] {
    border: 3px rgba(0,0, 255, 0.25) solid;
}

table[role=presentation]::before {
    content: "<table presentation>";
    background: rgba(0,0, 255, 0.25);
    color: black;
    display:block;
}


caption {
    border: 3px rgba(255,255, 0, 0.25) solid;
}

caption::before {
    content: "<caption>";
    background: rgba(255,255, 0, 0.25);
    color: black;
    display:block;
}

th {
    border: 3px rgba(255,0, 255, 0.25) solid;
}

th::before {
    content: "<th>";
    background: rgba(255,0, 255, 0.25);
    color: black;
    display:block;
}
th[scope]::after {
    content: "scope " attr(scope);
    background: rgba(100,100, 255, 0.5);
    color: black;
    display:block;
}
```
